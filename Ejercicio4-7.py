# Ejercicio 7: Reescribe el programa de calificaciones del capítulo
# anterior usando una función llamada calcula_calificacion, que
# reciba una puntuación como parámetro y devuelva una calificación
# como cadena.

# Autor: Brayan Gonzalo Cabrera Cabrera.

# brayan.cabrera@unl.edu.ec




puntuación = float (input("Introduzca puntuación:"))
    # validación del rango de la puntuación

def calculo_calificacion(puntuación):
    if puntuación >= 0 and puntuación <= 1.0:

        if puntuación >= 0.9:
            print("Sobresaliente")
        elif puntuación >= 0.8:
            print("Notable")
        elif puntuación >= 0.7:
            print("Bien")
        elif puntuación >= 0.6:
            print("Suficiente")
        elif puntuación < 0.6:
            print("Insuficiente")
    else:
        print("Puntuación Incorrecta")
calculo_calificacion(puntuación)


